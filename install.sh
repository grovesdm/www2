#!/bin/bash
###################################################################################
# SETTINGS
###################################################################################
NOW="$(date +"%Y-%m-%d_%H:%M:%S")"
################
# FILE LOCATIONS
################
WWWROCKET_DIRECTORY="/etc/wwwrocket"
ROCKET_DIRECTORY="${WWWROCKET_DIRECTORY}/rocket"
LOG="${WWWROCKET_DIRECTORY}/rocket.log"
ANSIBLE_HOSTS="${ROCKET_DIRECTORY}/ansible/roles/INSTALL/files/etc/ansible/hosts.yml"
DEFAULT_CONFIG="${ROCKET_DIRECTORY}/default.conf"
CUSTOM_CONFIG="${WWWROCKET_DIRECTORY}/custom.conf"
##################################################
# CHECK FOR CUSTOM CONFIG FILE ###################
##################################################
FILE="${CUSTOM_CONFIG}" && test -f $FILE && source $FILE
########
# COLORS
########
RED=`tput setaf 1`
GREEN=`tput setaf 2`
HIGHLIGHT=`tput smso`
YELLOW=`tput setaf 3`
RESET=`tput sgr0`
###################################################################################
# MESSAGES ########################################################################
###################################################################################
ERROR_RUN_AS_ROOT="\n${RED}WWWROCKET ERROR =>${RESET} ${HIGHLIGHT} SORRY THIS SCRIPT MUST BE RUN AS SUDO OR ROOT ${RESET}\n\n"
ERROR_VAR="\n${RED}WWWROCKET ERROR =>${RESET} ${HIGHLIGHT} SORRY UNKNOWN VARIABLE. AVAILABLE OPTION IS ${RESET} ${YELLOW} --clean ${RESET}\n\n"
REMOVING_PACKAGES="\n${YELLOW} REMOVING EXISTING PACKAGES =>${RESET} ${HIGHLIGHT} PYTHON PIP ANSIBLE GIT ${RESET}\n\n"
INSTALLING_PYTHON="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING PYTHON & PIP ${RESET}\n\n"
INSTALLING_ANSIBLE="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING ANSIBLE WITH PIP ${RESET}\n\n"
INSTALLING_GIT="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING GIT ${RESET}\n\n"
HANDOVER_TO_WWWROCKET="\n${GREEN} PREPARING FOR LAUNCH => ${RESET}${HIGHLIGHT} HANDING OVER TO WWWROCKET INSTALLER ${RESET}\n\n"
###################################################################################
# CHECK RUNNING AS ROOT ###########################################################
###################################################################################
if (( $EUID != 0 )); then
    echo -ne "${ERROR_RUN_AS_ROOT}"
    exit 1
fi
##################################################
# CHECK FOR --PURGE VARIABLE #####################
##################################################
if [[ "$1" == "--purge" ]]; then
    echo -ne "${REMOVING_PACKAGES}"
    pip uninstall ansible -q
    apt -qq remove git python python-pip python-simplejson --purge -y
    apt -qq autoremove -y
    echo -ne "\n${NOW}\n * REMOVED PYTHON PIP ANSIBLE & GIT\n" >> ${LOG}
##################################################
# CHECK FOR NON EMPTY OR INCORRECT VARIABLE ######
##################################################
elif [[ ! -z "$1" ]] && [[ "$1" != "--purge" ]] ; then
    echo -ne "${ERROR_VAR}"
    exit 1
fi
##################################################
# CREATE ROCKET DIRECTORY FOR LOG FILE ###########
##################################################
mkdir -p "${WWWROCKET_DIRECTORY}"
echo -ne "\n${NOW}\n * RUNNING LAUNCHPAD\n" >> ${LOG}
##################################################
# INSTALLATION ###################################
##################################################
python_install()
{
    echo -ne "${INSTALLING_PYTHON}"
    apt update
    apt install python-simplejson python-pip -y
    echo -ne " * INSTALLED PYTHON & PIP\n" >> ${LOG}
}
ansible_install()
{
    echo -ne "${INSTALLING_PACKAGE}"
    pip install ansible -q
    mkdir -p /etc/ansible
    echo -ne " * INSTALLED ANSIBLE\n" >> ${LOG}
}
git_install()
{
    echo -ne "${INSTALLING_PACKAGE}"
    apt-get update
    apt-get install git -y
    echo -ne " * INSTALLED GIT\n" >> ${LOG}
}
##################################################
# CHECK REQUIRED PACKAGES INSTALLED ##############
##################################################
dpkg -s "python-simplejson" >/dev/null 2>&1 || python_install
pip show "ansible" >/dev/null 2>&1 || ansible_install
dpkg -s "git" >/dev/null 2>&1 || git_install 
##################################################
# REMOVE ANY PREVIOUS INSTALLS ###################
##################################################
rm -rf "${ROCKET_DIRECTORY}"
echo -ne " * CLEARED ANY PREVIOUS WWWROCKET INSTALLS\n" >> ${LOG}
##################################################
# CLONE WWWROCKET REPOSITORY #####################
##################################################
git clone https://bitbucket.org/grovesdm/www2.git ${ROCKET_DIRECTORY}
echo -ne " * CLONED WWWROCKET GIT REPOSITORY\n" >> ${LOG}
##################################################
# UPDATE ANSIBLE HOSTS ###########################
##################################################
cat "${ANSIBLE_HOSTS}" > /etc/ansible/hosts
echo -ne " * REPLACE DEFAULT ANSIBLE HOSTS FILE\n" >> ${LOG}
##################################################
# START THE WWWROCKET INSTALLATION ###############
##################################################
echo -ne "${HANDOVER_TO_WWWROCKET}"
echo -ne " * LAUNCHPAD COMPLETED... HANDING OVER TO ANSIBLE\n" >> ${LOG}
ansible-playbook "${ROCKET_DIRECTORY}/ansible/install.yml"
# bash "${ROCKET_DIRECTORY}/install.sh"


# Now that the git is there I can surce the config



# ref the conf file
source "${DEFAULT_CONFIG}" # In case i need any vars

# So it grabs the custom config again...
#################################################
##################################################
# CHECK FOR CUSTOM CONFIG FILE ###################
##################################################
FILE="${CUSTOM_CONFIG}" && test -f "${FILE}" && source "${FILE}"


# MAKE ALL OF THE REQUIRED DIRECTORIES
# mkdir -p $AI_FOLDER
mkdir -p "${TMP_FOLDER}"


# BACKUP FOLDERS
mkdir -p "${BACKUP_FOLDER}"
mkdir -p "${AI_CONFIG_BACKUP_FOLDER}"
mkdir -p "${VHOST_BACKUP_FOLDER}"
mkdir -p "${SITE_BACKUP_FOLDER}"
mkdir -p "${DB_BACKUP_FOLDER}"

# PASSWORD FOLDERS
mkdir -p "${PASSWORDS_FOLDER}"
mkdir -p "${DB_PASSWORDS_FOLDER}"
mkdir -p "${FTP_PASSWORDS_FOLDER}"
mkdir -p "${WP_PASSWORDS_FOLDER}"




# Create symbolic links to the script #############
ln -sf /etc/wwwrocket/rocket/scripts/www.sh /usr/local/bin/www
ln -sf /etc/wwwrocket/rocket/scripts/www.sh /usr/local/bin/ai

# Make it executable and include support for ai as I am an idiot
chmod +x /usr/local/bin/www
chmod +x /usr/local/bin/ai


# Creating default 000-default config
bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/vhost/default.sh"
#######################
# Finish up
# clear &&

ok "\n  Installation has completed...\n" &&

ok "\n  Ok thats done.... What now? Type ai for suggestions... \n\n"


exit 1
