# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Site Module:
- Changed help file to latest commands.
- Created command to access help file.
- Changed enable/ disable sites task changed from Apache to Nginx
- Removed edit and reload functions
- Changed location of site backup and restore scripts
- Changed backup restore scripts and tidy code to fix bugs since move.



## 1.92 - 



---

## 1.9.1 - 2018-04-21
- Added new help files to each module. 
- Changed all modules to have the same structure:
```shell
Module
    ├── help.md
    ├── main.sh
    └── tasks
        ├── do_something.sh
        └── do_something_else.sh
```
- Removed update section and other old code from installer

[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
[1.0.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0
