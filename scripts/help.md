
  
  
  
    MODULES     USAGE               OPTIONS
    =======     =====               =======

    BACKUP      www backup ...
    DATABASE    www database ...
    FIREWALL    www firewall ...
    FTP         www ftp ...
    HELP        www module help
    SITE        www site ...      --add --DELETE --enable --disable --backup --RESTORE

Access the help for each module with www module_name --help
    
    HELPERS             USAGE
    =======             =====

    MANAGE SERVICES     www apache|nginx|web|firewall|database start|stop|restart|reload|status
    FILE NAVIGATION     www cd www|backup|passwords|rocket ...
    EDIT FILES          www edit apache|nginx ... (--ssl)
    INSTALLER           www rocket update|reinstall (optional --purge)
    TOOLS               www tools ports|
    
    For more info please visit: https://www.sh/docs

www rocket --update|--reinstall    (--purge)