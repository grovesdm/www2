#!/usr/bin/env bash

#################################################
# LOAD CONFIG ###################################
#################################################
source "/etc/wwwrocket/rocket/default.conf"

#################################################
# SITE LIST #####################################
#################################################
if  [ "$2" = '--list' ]; then
	bash "${SCRIPTS_DIRECTORY}/site/tasks/list.sh" "$@"
	exit 1

#################################################
# LIST AVAILABLE BACKUPS ########################
#################################################
elif [ "$2" = '--list-backups' ]; then
  echo -e "\033[0m"
  echo "AVAILABLE BACKUPS"
  ls $SITE_BACKUP_FOLDER -I html -I index.html | col -b
exit 1


#################################################
# SITE HELP #####################################
#################################################
elif [ "$2" = '--help' ]; then
cat "${SCRIPTS_DIRECTORY}/site/help.md"

#################################################
# SITE DISABLE ##################################
#################################################
elif [ "$3" = '--disable' ]; then
bash "${SCRIPTS_DIRECTORY}/site/tasks/nginx/site-disable.sh" "$2"

#################################################
# SITE ENABLE ###################################
#################################################
elif [ "$3" = '--enable' ]; then
bash "${SCRIPTS_DIRECTORY}/site/tasks/nginx/site-enable.sh" "$2"

#################################################
# SITE ADD WITH FTP ############################# #todo Need to fix this as it is not tested and will be broken
#################################################
elif [[ "$3" = "--add" ]]; then

    if [ "$4" = '--ftp' ] || [ "$5" = '--ftp' ]; then
        warn "I will create site $2 and ftp user $2" &&
        bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-add.sh" "$2" &&
        bash "${SCRIPTS_DIRECTORY}/ftp/tasks/user-add.ftp" "$2"

        ok "
        Done and done... Created site $2 and ftp user $2
        "


    #################################################
    ## SITE ADD WITHOUT FTP #########################
    #################################################

    # Passes FQDN to add
    else
        bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-add.sh" "$2" "$4" "$5"


    fi

#################################################
# SITE DELETE WITH FTP ##########################
#################################################
elif [ "$3" = '--DELETE' ] && [ "$4" = '--ftp' ]; then
bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-delete.sh" "$2"
bash "${SCRIPTS_DIRECTORY}/ftp/tasks/user-delete.sh" "$2"

echo "
deleted with ftp
"

#################################################
# SITE DELETE WITHOUT FTP #######################
#################################################
elif [ "$3" = '--DELETE' ]; then
bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-delete.sh" "$2"
echo "
deleted $2 without ftp
"
##########################






##### htdocs backup ######################################################


elif [ "$3" = '--backup' ] && [ "$1" = 'site' ]; then

    bash "${SCRIPTS_DIRECTORY}/site/tasks/backup/htdocs_backup.sh" "$2" "$4"


##### htdocs restore ######################################################
elif [ "$3" = '--RESTORE' ] && [ "$1" = 'site' ]; then

    bash "${SCRIPTS_DIRECTORY}/site/tasks/backup/htdocs_restore.sh" "$2"



fi
