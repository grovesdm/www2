#!/bin/bash
##################################################
# Redirect to with or without www ################
##################################################

# Load Config
source "/etc/wwwrocket/rocket/default.conf"
#TLD="$(echo "$1" | sed -r 's/www\.//')"

if [ "$1" != "$TLD" ]; then # if www

echo "
# Redirect from $TLD to $1
<VirtualHost *:8080>
	ServerName $TLD
	# Add alias as required for example
	#ServerAlias www1.$TLD www2.$TLD
	Redirect 301 / http://$1/

	# To force SSL
	#Redirect 301 / https://$1/
</VirtualHost>
" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD$SITEVHOSTDIREXT


else # if not www

echo "
# Redirect from www.$TLD to $TLD
<VirtualHost *:8080>
	ServerName www.$TLD
	# Add alias as required for example
	#ServerAlias www1.$TLD www2.$TLD
	Redirect 301 / http://$TLD/

	# To force SSL
	#Redirect 301 / https://$1/
</VirtualHost>
" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD$SITEVHOSTDIREXT

fi


echo "

<VIRTUALHOST *:8080>
	ServerAdmin webmaster@$TLD
	ServerName $1
	DocumentRoot $WEBROOT/$TLD/htdocs
	#Redirect 301 / https://$1/

############################################################################
#### These should be set in the default Apache config but better to be safe
		<DIRECTORY "/">
			# Ensure that files outside the web root are not served
			Require all denied
		</DIRECTORY>

		<Files ".ht*">
			# Deny read access to .htaccess files
			Require all denied
		</Files>
############################################################################

	<DIRECTORY $WEBROOT/$TLD/htdocs>

		# Allow public access to site
		Require all granted

		# Turn off directory browsing
		Options -Indexes

		# Read .htaccess AllowOverride None = no and AllowOverride All = yes
		# AllowOverride None
		AllowOverride All

	</DIRECTORY>

	ErrorLog $WEBROOT/$TLD/$SITELOGS/apache_error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

    # Custom Log location + options
	CustomLog $WEBROOT/$TLD/$SITELOGS/apache_access.log combined env=!dontlog
</VIRTUALHOST>

# If you are using POUND -> VARNISH -> APACHE to serve SSL
#uncomment the next line and make sure there are no SSL redirects above
#SetEnvIf X-Forwarded-Proto "^https$" HTTPS=on

# For normal SSL on default port 443
# Set redirects above as required to force SSL on the page
# SSL config is set to 'AllowOverride All' by default
#Include $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD-ssl$SITEVHOSTDIREXT

" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD$SITEVHOSTDIREXT
