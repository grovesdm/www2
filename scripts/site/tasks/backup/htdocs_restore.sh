#!/usr/bin/env bash
#################################################
# LOAD CONFIG ###################################
#################################################
source "/etc/wwwrocket/rocket/default.conf"
#################################################
# MESSAGES ######################################
#################################################
BACKUPS=( "$SITE_BACKUP_FOLDER/$TLD"*.zip )
BACKUP_SELECT="\n${GREEN}  WWWROCKET =>${RESET} ${HIGHLIGHT} SELECT THE BACKUP YOU WANT TO RESTORE OR PRESS Q TO QUIT ${RESET}\n\n"
BACKUP_NONE="\n${RED}  WWWROCKET =>${RESET} ${HIGHLIGHT} SORRY THERE ARE NO BACKUPS FOR THIS SITE [$TLD] ${RESET} - MAYBE CHECK SPELLING? \n\n"
BACKUP_SELECT_FAIL="\n${RED}  WWWROCKET =>${RESET} ${HIGHLIGHT} SORRY THE BACKUP YOU HAVE SELECTED DOES NOT EXIST. PLEASE SELECT AGAIN ${RESET}\n\n"
RESTORE_CONFIRM="\n${GREEN}  WWWROCKET =>${RESET} ${HIGHLIGHT} CONFIRM YOU WANT TO RESTORE BACKUP FOR [$1]. IT WILL OVERWRITE IF IT EXISTS ${RESET}\n\n"
RESTORE_CANCELLED="\n${YELLOW}  WWWROCKET =>${RESET} ${HIGHLIGHT} BACKUP RESTORATION CANCELLED ${RESET}\n\n"
#################################################
# CHECK IF A BACKUP EXISTS IN THE BACKUP FOLDER #
#################################################
if ls $SITE_BACKUP_FOLDER/$TLD*.zip 1> /dev/null 2>&1; then
    #################################################
    # IF BACKUPS EXISTS ASK USER TO SELECT ##########
    #################################################
    echo -ne "$BACKUP_SELECT"
    select BACKUP in "${BACKUPS[@]}"; do
        #################################################
        # IF USER QUITS #################################
        #################################################
        if [[ $REPLY =~ ^[Qq]$ ]]; then
            echo -ne "$RESTORE_CANCELLED" >&2
            exit 1
        #################################################
        # IF USER SELECTS INVALID OPTION ################
        #################################################
        elif [[ -z $BACKUP ]]; then
            echo -ne "$BACKUP_SELECT_FAIL" >&2
        #################################################
        else
            #################################################
            # CONFIRMATION BEFORE RESTORE ###################
            #################################################
            echo -ne "$RESTORE_CONFIRM"
            #################################################
            # CALL CONFIRMATION FUNCTION ####################
            #################################################
            confirm
            ok "\n RESTORING $BACKUP_NAME FOR SITE [$1]\n\n"
            #################################################
            # RUN WWWROCKET SITE ADD ########################
            #################################################
            #################################################
            # FIRST CHECK FOR SSL CERTFICATE ################
            #################################################
            if [ -f /etc/letsencrypt/renewal/$TLD.conf ]; then
                #################################################
                # SITE HAS SSL CERTFICATE #######################
                #################################################
                www site $1 --add --ssl
                #################################################
            else
                #################################################
                # SITE HAS NO SSL CERTFICATE ####################
                #################################################
                www site $1 --add
                #################################################
            fi
            #################################################
            # MOVE OUT OF SITE DIRECTORY ####################
            #################################################
            cd $WEBROOT
            #################################################
            # REMOVE EXISTING SITE FOR CLEAN RESTORE ########
            #################################################
            rm -fr "$WEBROOT/$TLD"
            #################################################
            # EXTRACT BACKUP ################################
            #################################################
            unzip -o $BACKUP -d $WEBROOT
            #################################################
            # CONFIRM COMPLETED #############################
            #################################################
            ok "\n RESTORE COMPLETED\n\n"
            #################################################
            # RESET DIRECTORY OWNERSHIP AND PERMISSIONS ##### #todo Need to audit the permissions file
            #################################################
            bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-permissions.sh" $TLD
            #################################################
            # RESTART NGINX AND APACHE ###################### #todo Change this to a function
            #################################################
            service apache2 restart
            service nginx restart
            #################################################
            # END ###########################################
            #################################################
            exit 1
            #################################################
        fi
    done
#################################################
# IF NO TLD BACKUPS EXIST #######################
#################################################
else
    echo -ne $BACKUP_NONE
    exit 1
#################################################
fi
