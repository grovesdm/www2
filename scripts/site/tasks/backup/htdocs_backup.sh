#!/usr/bin/env bash
#################################################
# LOAD CONFIG ###################################
#################################################
source "/etc/wwwrocket/rocket/default.conf"
#
## test for dev
#source "/home/daniel/Documents/www2/default.conf"

if [[ $2 != "" ]]; then
  SITE_BACKUP_DESCRIPTION="--$2"
fi

SITE_BACKUP_NAME="$TLD--$BACKUP_TIME$SITE_BACKUP_DESCRIPTION.zip"


#################################################
# MESSAGES ######################################
#################################################
BACKUP_SUCCESS="\n${GREEN}  WWWROCKET =>${RESET} ${HIGHLIGHT} BACKUP [$SITE_BACKUP_NAME] CREATED SUCCESSFULLY ${RESET}\n\n"
SITE_NOT_EXIST="\n${RED}  WWWROCKET =>${RESET} ${HIGHLIGHT} SORRY THIS SITE DOES NOT EXIST [$TLD] ${RESET} - MAYBE CHECK SPELLING? \n\n"



#################################################
# CHECK IF SITE FOLDER EXISTS ###################
#################################################
if [ -d "$WEBROOT/$TLD" ]; then
#################################################
# REMOVE FILES FROM SITE FIRST ##################
#################################################
# Remove iwp backups
#find $WEBROOT/tt.slyfoxmedia.com.au/htdocs/wp-content/infinitewp/backups/ -type f -not -name '*.php' -delete

if [ -d "$WEBROOT/$TLD/htdocs/wp-content/infinitewp/backups" ]; then
  find $WEBROOT/$TLD/htdocs/wp-content/infinitewp/backups/ -type f -not -name '*.php' -delete -nowarn
fi

if [ -d "$WEBROOT/$TLD/cache" ]; then
  rm $WEBROOT/$TLD/cache -R
fi
#################################################
# MOVE OUT OF SITE FOLDER AND REMOVE CACHE AND ZIP SITE
#################################################
cd $WEBROOT && zip -r "$SITE_BACKUP_FOLDER/$SITE_BACKUP_NAME" $TLD &&

#################################################
# CONFIRM #######################################
#################################################
echo -ne $BACKUP_SUCCESS

else
# If no exist
echo -ne $SITE_NOT_EXIST
exit 1
fi