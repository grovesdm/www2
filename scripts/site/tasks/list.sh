#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"


echo -e "\033[32m
Sites in the www folder
=======================" &&
echo -e "\033[0m"

ls $WEBROOT -I html -I index.html | col -b
echo ""

echo -e "\033[32m
Enabled sites:
==============

" &&
echo -e "\033[0m"
echo "APACHE"
ls /etc/apache2/sites-enabled -I html -I index.html | col -b
echo "NGINX"
ls /etc/nginx/sites-enabled -I html -I index.html | col -b
echo ""
