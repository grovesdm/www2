#!/bin/bash
##############################################################
# DEVROCKET SCRIPT TO CREATE NGINX VHOST #####################
##############################################################

# LOAD GLOBAL CONFIG ##############################################
source "/etc/wwwrocket/rocket/default.conf"
source "../site-add.sh"
###################################################################

echo"

  ##################################################
  # SHARED HTTP & HTTPS CONFIG #####################
  ##################################################
  root $WEBROOT/$TLD/htdocs;
  index index.php index.htm index.html;

    set \$no_cache 0;
    # Example: Don't cache admin area
    # Note: Conditionals are typically frowned upon :/
    if (\$request_uri ~* "/\(wp-admin/\)")
    {
        set \$no_cache 1;
    }


  location / {
    ##################################################
    # HTTP CONFIG ####################################  <---- DRAFT
    ##################################################
    try_files \$uri \$uri/ /index.php;
    gzip_static on;
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    ##################################################
    # HTTP CACHE CONFIG ##############################  <---- DRAFT
    ##################################################
    proxy_cache         $CACHE_ZONE;
    #proxy_cache_key    \$scheme\$proxy_host\$request_uri;
    #proxy_cache_valid 200 302 60m;
    #proxy_cache_valid 404 10m


  }






#  location ~* \.php$ {
#    ##################################################
#    # CUSTOM PHP CONFIG ##############################  <---- DRAFT
#    ##################################################
#    gzip on;
#    gzip_types application/xhtml+xml;
#    fastcgi_pass unix:/run/php/php7.2-fpm.sock;
#    include snippets/fastcgi-php.conf;
#  }


location ~ ^/(index)\.php(/|$) {
            fastcgi_cache $CACHE_ZONE_FCGI;
            fastcgi_cache_valid 200 60m; # Only cache 200 responses, cache for 60 minutes
            fastcgi_cache_methods GET HEAD; # Only GET and HEAD methods apply
            add_header X-Fastcgi-Cache \$upstream_cache_status;
            fastcgi_cache_bypass \$no_cache;  # Don't pull from cache based on \$no_cache
            fastcgi_no_cache \$no_cache; # Don't save to cache based on \$no_cache
#            fastcgi_cache_key "\$scheme\$request_method\$host\$request_uri";
            # Regular PHP-FPM stuff:
            include fastcgi.conf; # fastcgi_params for nginx < 1.6.1
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
            fastcgi_index index.php;
#            fastcgi_param LARA_ENV production;
    }


  ################################
  # REFUSE ACCESS TO HTACCESS ####
  ################################
  location ~/\. {
      deny all;
      access_log off;
      log_not_found off;
  }

  access_log $WEBROOT/$TLD/$SITELOGS/access_nginx.log;
  error_log $WEBROOT/$TLD/$SITELOGS/error_nginx.log;

}

" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf