#!/bin/bash
##############################################################
# DEVROCKET SCRIPT TO CREATE NGINX VHOST #####################
##############################################################

# LOAD GLOBAL CONFIG ##############################################
source "/etc/wwwrocket/rocket/default.conf"
###################################################################

##############################################################
# SANITY CHECK ###############################################
##############################################################
[ $(id -g) != "0" ] && die "Script must be run as root."
[ $# != "2" ] && die "Usage: $(basename $0) domainName" #todo dont know why this is checking that only 1 var is passed...?


##############################################################
# Functions ##################################################
##############################################################
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }


# ##############################################################
# # VARIABLES ################################################## #todo these aere in config file. dont know why they are here.
# ##############################################################
# NGINX_AVAILABLE_VHOSTS='/etc/nginx/sites-available'
# NGINX_ENABLED_VHOSTS='/etc/nginx/sites-enabled'
# WEB_DIR='/var/www'
# WEB_USER='www-data'
# USER='wwwrocket'
NGINX_SCHEME='$scheme'
NGINX_REQUEST_URI='$request_uri'
CACHE_ZONE="$(echo "$TLD" | sed -r 's/[.]+/_/g')"
CACHE_ZONE_FCGI="$(echo "$TLD" | sed -r 's/[.]+/_/g')_fcgi"
SSL=FALSE
SSL_REDIRECT=""

# SET DOMIAN VARS
if [ "$1" == "www.$TLD" ]; then
  DOMAIN=www.$TLD
  REDIRECT=$TLD
else
  DOMAIN=$TLD
fi

if [ "$2" = "--ssl" ]; then # IF SITE IS SSL
SSL=TRUE
SSL_REDIRECT="return 301 https://$DOMAIN\$request_uri;"
fi

#if [ -f "$WEBROOT/$TLD/config/custom.conf" ]; then
#  source $WEBROOT/$TLD/config/custom.conf
#fi



#fi

if [ -f "$WEBROOT/$TLD/config/custom.conf" ]; then
  source "$WEBROOT/$TLD/config/custom.conf"

elif [ ! -f "$WEBROOT/$TLD/config/custom.conf" ]; then

# IF CUSTOM COF NOT EXISTS
cat > $WEBROOT/$TLD/config/custom.conf <<EOF
#################################################
# SET ALTERNATE HOSTNAMES #######################
#################################################
ALT_NAMES=""
#################################################
# http://localhost:8080 is default
PROXY_BACKEND=""
#################################################

#################################################
# LOCK VHOST TRUE|FALSE #########################
#################################################
LOCK=FALSE
EOF

fi



##############################################################
# SHARED DEFAULT VHOST CONFIG ################################
##############################################################
DEFAULT="
  ##################################################
  # DEFAULT VHOST CONFIG ###########################
  ##################################################
  root $WEBROOT/$TLD/htdocs;
  index index.php index.htm index.html;

  set \$no_cache 0;
  # Example: Don't cache admin area
  # Note: Conditionals are typically frowned upon :/
  if (\$request_uri ~* '/(wp-admin/)')
  {
      set \$no_cache 1;
  }


  location / {
    ##################################################
    # ALL DOMAINS #################################### <---- DRAFT
    ##################################################
    gzip_static on;
    try_files \$uri \$uri/ /index.php;
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Forwarded-Proto https;

    ##################################################
    # CACHE CONFIG ###################################  <---- DRAFT
    ##################################################
    proxy_cache         $CACHE_ZONE;
    proxy_cache_key    \$scheme\$proxy_host\$request_uri;
    proxy_cache_valid 200 301 302 60m;
    proxy_cache_valid 404 10m;
    add_header X-Proxy-Cache \$upstream_cache_status;
  }

  location ~ ^/(index)\.php(/|$) {
    ##################################################
    # PHP ############################################ <---- DRAFT
    ##################################################
    gzip on;
    gzip_types application/xhtml+xml;

    ##################################################
    # FASTCGI CONFIG ################################# <---- DRAFT
    ##################################################
    # CACHE
    fastcgi_cache $CACHE_ZONE_FCGI;
    fastcgi_cache_key \$scheme\$request_method\$host\$request_uri;
    fastcgi_cache_valid 200 301 302 60m; # Only cache 200, 301, 302 responses, cache for 60 minutes
    fastcgi_cache_methods GET HEAD; # Only GET and HEAD methods apply
    add_header X-Fastcgi-Cache \$upstream_cache_status;
    fastcgi_cache_bypass \$no_cache;  # Don't pull from cache based on \$no_cache
    fastcgi_no_cache \$no_cache; # Don't save to cache based on \$no_cache

    # Regular PHP-FPM stuff:
#    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    include snippets/fastcgi-php.conf;
  }


  ################################
  # REFUSE ACCESS TO HIDDEN FILES
  ################################
  location ~/\. {
      deny all;
      access_log off;
      log_not_found off;
  }

  access_log $WEBROOT/$TLD/$SITELOGS/nginx_access.log;
  error_log $WEBROOT/$TLD/$SITELOGS/nginx_error.log;
}
"
##############################################################
# END SHARED CONFIG ##########################################
##############################################################


echo "

#TLD=$TLD
#DOMAIN=$DOMAIN
#REDIRECT=$REDIRECT

##############################################################
# SETUP CACHE ZONES ##########################################
##############################################################
proxy_cache_path $WEBROOT/$TLD/cache levels=1:2 keys_zone=$CACHE_ZONE:10m max_size=1000m inactive=720m;
#proxy_cache_key "\$scheme\$request_method\$host\$request_uri";
fastcgi_cache_path $WEBROOT/$TLD/cache/fcgi levels=1:2 keys_zone=$CACHE_ZONE_FCGI:100m inactive=60m;
#fastcgi_cache_key \$scheme\$request_method\$host\$request_uri;
" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

if [ "$1" == "www.$TLD" ]; then

echo "
server {
##################################################
# REDIRECT TO CORRECT SERVER NAME ################
##################################################
  listen 80;
  listen [::]:80;
  server_name $REDIRECT;
  return 301 http://$DOMAIN\$request_uri;
}
" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

fi

echo "

server {
##################################################
# HTTP CONFIG ####################################
##################################################
  listen 80;
  listen [::]:80;
  server_name $DOMAIN $ALT_NAMES;
  $SSL_REDIRECT

  $DEFAULT

  " >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf



if [ "$SSL" = "TRUE" ]; then # IF SITE IS SSL

if [ "$1" == "www.$TLD" ]; then

echo "
server {
##################################################
# REDIRECT TO CORRECT SERVER NAME ################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name $REDIRECT;
  return 301 https://$DOMAIN\$request_uri;
  ssl_certificate /etc/letsencrypt/live/$TLD/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$TLD/privkey.pem;
}
" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf
fi

echo "
server {
##################################################
# HTTPS CONFIG ###################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name $DOMAIN $ALT_NAMES;

  ssl_certificate /etc/letsencrypt/live/$TLD/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$TLD/privkey.pem;

  ssl_session_cache shared:SSL:20m;
  ssl_session_timeout 60m;
  
  ### NEEDS MORE RESEARCH AND CONFIG
  #ssl_prefer_server_ciphers on;
  #ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DHE+AES128:!ADH:!AECDH:!MD5;
  #ssl_dhparam /etc/nginx/cert/dhparam.pem;
  #ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

  ssl_stapling on;
  ssl_stapling_verify on;
  #ssl_trusted_certificate /etc/nginx/cert/trustchain.crt;
  resolver 8.8.8.8 8.8.4.4;

  # Uncomment this line only after testing in browsers,
  # as it commits you to continuing to serve your site over HTTPS
  # in future
  # add_header Strict-Transport-Security "max-age=31536000";

  $DEFAULT

" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf


fi



ln -sf $WEBROOT/$TLD/config/$TLD.nginx.conf $NGINX_AVAILABLE_VHOSTS/$TLD.conf

##############################################################
# ENABLE SITE BY CREATING SYMBOLIC LINK TO SITES ENABLED #####
##############################################################
ln -sf $NGINX_AVAILABLE_VHOSTS/$TLD.conf $NGINX_ENABLED_VHOSTS/$TLD.conf


##############################################################
# RESTART NGINX AND SHOW STATUS ##############################
##############################################################
/etc/init.d/nginx restart;
ok "Site Created for $1"
/etc/init.d/nginx status
exit 1