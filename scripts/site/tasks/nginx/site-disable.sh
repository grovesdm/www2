#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"

# -z str: Returns True if the length of str is equal to zero.
if [ -z "$TLD" ]; then
    echo "Please choose a site."
    exit 1
else
    echo "Disabling site $TLD..."
    # -h filename: True if file exists and is a symbolic link.
    # -f filename: Returns True if file, filename is an ordinary file.
    if [ ! -h "$NGINX_ENABLED_VHOSTS/$TLD$EXTENSION" ] && [ ! -f "$NGINX_ENABLED_VHOSTS/$TLD$EXTENSION" ]; then
        echo "$TLD is not enabled. $NGINX_ENABLED_VHOSTS/$TLD$EXTENSION"
        exit 1
    else
        rm $NGINX_ENABLED_VHOSTS/$TLD$EXTENSION
        service nginx restart
        service apache2 restart
        echo "Disabled $TLD "
    fi
fi