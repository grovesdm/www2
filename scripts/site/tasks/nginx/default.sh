#!/bin/bash
##############################################################
# DEVROCKET SCRIPT TO CREATE NGINX VHOST #####################
##############################################################
# Load Config
source "/etc/wwwrocket/rocket/default.conf"
##############################################################
# CREATE NGINX VHOST FILE ####################################
##############################################################
echo "
#hello
server {
##################################################
# HTTP CONFIG ####################################
##################################################
  listen 80 default_server;
  listen [::]:80 default_server localhost;
  server_name _;
  #return 301 https://\$server_name\$request_uri;
  root /var/www/000-default/htdocs;
  index index.php index.htm index.html;

  location / {
    try_files \$uri \$uri/ @php;

    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

  }

  location @php {
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    include snippets/fastcgi-php.conf;
    fastcgi_pass localhost:9000;
  }

  ################################
  # REFUSE ACCESS TO HTACCESS ####
  ################################
  location ~ /\.ht {
    deny all;
  }
}

" > /var/www/000-default/config/default.nginx.conf

ln -sf /var/www/000-default/config/default.nginx.conf $NGINX_AVAILABLE_VHOSTS/default


##############################################################
# ENABLE SITE BY CREATING SYMBOLIC LINK TO SITES ENABLED #####
##############################################################
ln -sf $NGINX_AVAILABLE_VHOSTS/default $NGINX_ENABLED_VHOSTS/default


##############################################################
# RESTART NGINX AND SHOW STATUS ##############################
##############################################################
/etc/init.d/nginx restart;
ok "Site Created for default"
/etc/init.d/nginx status