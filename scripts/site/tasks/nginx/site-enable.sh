#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"

# -z str: Returns True if the length of str is equal to zero.
if [ -z "$TLD" ]; then
    echo "Please choose a site."
    exit 1
else
    echo "Enabling site $TLD..."
    # -h filename: True if file exists and is a symbolic link.
    # -f filename: Returns True if file, filename is an ordinary file.
    if [ -h "$NGINX_ENABLED_VHOSTS/$TLD$EXTENSION" ] || [ -f "$NGINX_ENABLED_VHOSTS/$TLD$EXTENSION" ]; then
        echo "$TLD is already enabled. $NGINX_ENABLED_VHOSTS$TLD$EXTENSION"
        exit 1
    else
        if [ ! -f "$NGINX_AVAILABLE_VHOSTS/$TLD$EXTENSION" ]; then
            echo "Site $TLD does not exist in $NGINX_AVAILABLE_VHOSTS "
            exit 1
        else
            ln -s $NGINX_AVAILABLE_VHOSTS/$TLD$EXTENSION $NGINX_ENABLED_VHOSTS/$TLD$EXTENSION
            service apache2 restart
            service nginx restart
            echo "Enabled $TLD"
            exit 0
        fi
    fi
fi