#!/bin/bash
##################################################
# RUN SCRIPT WITH WW FOR TLD #####################
##################################################

# Load Config
source "/etc/wwwrocket/rocket/default.conf"


##############################################################
# LIST ANY CERTS INSTALLED ###################################
##############################################################
if [ "$1" = "--list" ]; then
  echo -e "\033[0m"
  echo "INSTALLED SSL CERTS"
  ls /etc/letsencrypt/renewal -I html -I index.html | col -b
exit 1


elif [ "$1" != "$TLD" ] && [ "$2" = "--add" ]; then # if www
##############################################################
# CREATE A CERTBOT CERTIFICATE ###############################
##############################################################
certbot certonly --standalone -d $TLD -d $1 --agree-tos \
-m webmaster@$TLD --pre-hook "service nginx stop" \
--post-hook "service nginx start"
exit 1

elif [ "$1" = "$TLD" ] && [ "$2" = "--add" ]; then # if not www
##############################################################
# CREATE A CERTBOT CERTIFICATE ###############################
##############################################################
certbot certonly --standalone -d $TLD --agree-tos \
-m webmaster@$TLD --pre-hook "service nginx stop" \
--post-hook "service nginx start"
exit 1



elif [ "$2" = "--DELETE" ]; then
  certbot delete --cert-name $TLD
exit 1

#else


elif [ "$2" = "--test" ]; then
certbot certonly --standalone -d $TLD --agree-tos \
-m webmaster@$TLD --pre-hook "service nginx stop" \
--post-hook "service nginx start" --dry-run
exit 1

#else
  #







else
#elif [ "$3" = "--disable" ]; then
#  certbot delete --cert-name $1
#
#elif [ "$3" = "--enable" ]; then
  echo "xSORRY YOU NEED TO CHOOSE AN OPTION"
  exit 1
fi

echo "sent to email address webmaster@$TLD"
