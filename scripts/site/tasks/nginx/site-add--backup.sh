#!/bin/bash
##############################################################
# DEVROCKET SCRIPT TO CREATE NGINX VHOST #####################
##############################################################

# LOAD GLOBAL CONFIG ##############################################
source "/etc/wwwrocket/rocket/default.conf"
###################################################################

##############################################################
# SANITY CHECK ###############################################
##############################################################
[ $(id -g) != "0" ] && die "Script must be run as root."
[ $# != "2" ] && die "Usage: $(basename $0) domainName" #todo dont know why this is checking that only 1 var is passed...?


##############################################################
# Functions ##################################################
##############################################################
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }


# ##############################################################
# # VARIABLES ################################################## #todo these aere in config file. dont know why they are here.
# ##############################################################
# NGINX_AVAILABLE_VHOSTS='/etc/nginx/sites-available'
# NGINX_ENABLED_VHOSTS='/etc/nginx/sites-enabled'
# WEB_DIR='/var/www'
# WEB_USER='www-data'
# USER='wwwrocket'
NGINX_SCHEME='$scheme'
NGINX_REQUEST_URI='$request_uri'
CACHE_ZONE="$(echo "$1" | sed -r 's/[.]+/_/g')"
CACHE_ZONE_FCGI="$(echo "$1" | sed -r 's/[.]+/_/g')_fcgi"
DOMAIN_TLD=$TLD
DOMAIN=$1
##############################################################
# CREATE NGINX VHOST FILE ####################################
##############################################################

echo "
##############################################################
# SETUP CACHE ZONES ##########################################
##############################################################
proxy_cache_path $WEBROOT/cache/$1 levels=1:2 keys_zone=$CACHE_ZONE:10m max_size=1000m inactive=720m;
#proxy_cache_key "\$scheme\$request_method\$host\$request_uri";
#fastcgi_cache_path $WEBROOT/$1/cache levels=1:2 keys_zone=$CACHE_ZONE_FCGI:100m inactive=60m;
#fastcgi_cache_key "$scheme$request_method$host$request_uri";
" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf


if [ "$1" != "$TLD" ]; then # IF SITE IS NOT TLD

echo "

server {
##################################################
# HTTP REDIRECT FROM TLD TO WWW ##################
##################################################
  listen 80;
  listen [::]:80;
  server_name $TLD;
  return 301 http://$1\$request_uri;
}

server {
##################################################
# HTTP CONFIG ####################################
##################################################
  listen 80;
  listen [::]:80;
  server_name $1;" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

else # IF SITE IS TLD

echo "

server {
##################################################
# HTTP REDIRECT FROM WWW TO TLD ##################
##################################################
  listen 80;
  listen [::]:80;
  server_name www.$TLD;
  return 301 http://$TLD\$request_uri;
}

server {
##################################################
# HTTP CONFIG ####################################
##################################################
  listen 80;
  listen [::]:80;
  server_name $TLD;" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

fi

if [ "$2" = "--ssl" ]; then # IF SITE IS SSL

echo "
  # REDIRECT TO SSL
  return 301 https://\$server_name\$request_uri;" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf
fi

echo "
  ##################################################
  # STANDARD HTTP CONFIG ###########################
  ##################################################
  root $WEBROOT/$TLD/htdocs;
  index index.php index.htm index.html;

set \$no_cache 0;

    # Example: Don't cache admin area
    # Note: Conditionals are typically frowned upon :/
    if (\$request_uri ~* "/\(wp-admin/\)")
    {
        set \$no_cache 1;
    }





  location / {
    ##################################################
    # HTTP CONFIG ####################################  <---- DRAFT
    ##################################################
    try_files \$uri \$uri/ /index.php;
    gzip_static on;
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    ##################################################
    # HTTP CACHE CONFIG ##############################  <---- DRAFT
    ##################################################
    proxy_cache         $CACHE_ZONE;
    #proxy_cache_key    \$scheme\$proxy_host\$request_uri;
    #proxy_cache_valid 200 302 60m;
    #proxy_cache_valid 404 10m


  }






#  location ~* \.php$ {
#    ##################################################
#    # CUSTOM PHP CONFIG ##############################  <---- DRAFT
#    ##################################################
#    gzip on;
#    gzip_types application/xhtml+xml;
#    fastcgi_pass unix:/run/php/php7.2-fpm.sock;
#    include snippets/fastcgi-php.conf;
#  }


location ~ ^/(index)\.php(/|$) {
            fastcgi_cache $CACHE_ZONE_FCGI;
            fastcgi_cache_valid 200 60m; # Only cache 200 responses, cache for 60 minutes
            fastcgi_cache_methods GET HEAD; # Only GET and HEAD methods apply
            add_header X-Fastcgi-Cache \$upstream_cache_status;
            fastcgi_cache_bypass \$no_cache;  # Don't pull from cache based on \$no_cache
            fastcgi_no_cache \$no_cache; # Don't save to cache based on \$no_cache
#            fastcgi_cache_key "\$scheme\$request_method\$host\$request_uri";
            # Regular PHP-FPM stuff:
            include fastcgi.conf; # fastcgi_params for nginx < 1.6.1
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
            fastcgi_index index.php;
#            fastcgi_param LARA_ENV production;
    }






  ################################
  # REFUSE ACCESS TO HTACCESS ####
  ################################
  location ~/\. {
      deny all;
      access_log off;
      log_not_found off;
  }

  access_log $WEBROOT/$TLD/$SITELOGS/nginx_http_access.log;
  error_log $WEBROOT/$TLD/$SITELOGS/nginx_http_error.log;

}" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf



if [ "$2" = "--ssl" ]; then # IF SITE IS SSL

if [ "$1" != "$TLD" ]; then # IF SITE IS SSL
echo "
server {
##################################################
# HTTPS REDIRECT #################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name $TLD;
  return 301 https://$1\$request_uri;
  ssl_certificate /etc/letsencrypt/live/$TLD/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$TLD/privkey.pem;
}

server {
##################################################
# HTTPS CONFIG ###################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name $1;" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

else

echo "
server {
##################################################
# HTTPS REDIRECT #################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name www.$1;
  return 301 https://$TLD\$request_uri;
  ssl_certificate /etc/letsencrypt/live/$TLD/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$TLD/privkey.pem;
}

server {
##################################################
# HTTPS CONFIG ###################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name $TLD;" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

fi


echo "
  ssl_certificate /etc/letsencrypt/live/$TLD/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$TLD/privkey.pem;

  ssl_session_cache shared:SSL:20m;
  ssl_session_timeout 60m;
  
  ### NEEDS MORE RESEARCH AND CONFIG
  #ssl_prefer_server_ciphers on;
  #ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DHE+AES128:!ADH:!AECDH:!MD5;
  #ssl_dhparam /etc/nginx/cert/dhparam.pem;
  #ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

  ssl_stapling on;
  ssl_stapling_verify on;
  #ssl_trusted_certificate /etc/nginx/cert/trustchain.crt;
  resolver 8.8.8.8 8.8.4.4;

  # Uncomment this line only after testing in browsers,
  # as it commits you to continuing to serve your site over HTTPS
  # in future
  # add_header Strict-Transport-Security "max-age=31536000";

  ##################################################
  # STANDARD SSL CONFIG ############################
  ##################################################
  root $WEBROOT/$TLD/htdocs;
  index index.php index.htm index.html;

  location / {
    ##################################################
    # ALL DOMAINS #################################### <---- DRAFT
    ##################################################
    gzip_static on;
    try_files \$uri \$uri/ /index.php;
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Forwarded-Proto https;


    ##################################################
    # HTTPS CACHE CONFIG #############################  <---- DRAFT
    ##################################################

    proxy_cache         $CACHE_ZONE;
    proxy_cache_key    \$scheme\$proxy_host\$request_uri;
    proxy_cache_valid 200 302 60m;
    proxy_cache_valid 404 10m;




  }



  location ~* \.php$ {
    ##################################################
    # PHP ############################################ <---- DRAFT
    ##################################################
    gzip on;
    gzip_types application/xhtml+xml;

    ##################################################
    # FASTCGI CONFIG ############################################ <---- DRAFT
    ##################################################
    # CACHE
    fastcgi_cache $CACHE_ZONE_FCGI;
    fastcgi_cache_valid 200 60m; # Only cache 200 responses, cache for 60 minutes
    fastcgi_cache_methods GET HEAD; # Only GET and HEAD methods apply
    add_header X-Fastcgi-Cache \$upstream_cache_status;
    fastcgi_cache_bypass \$no_cache;  # Don't pull from cache based on \$no_cache
    fastcgi_no_cache \$no_cache; # Don't save to cache based on \$no_cache

    # Regular PHP-FPM stuff:
#    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    include snippets/fastcgi-php.conf;
  }

  ################################
  # REFUSE ACCESS TO HIDDEN FILES
  ################################
  location ~/\. {
      deny all;
      access_log off;
      log_not_found off;
  }

  access_log $WEBROOT/$TLD/$SITELOGS/nginx_ssl_access.log;
  error_log $WEBROOT/$TLD/$SITELOGS/nginx_ssl_error.log;
}

" >> $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD.nginx.conf

fi

ln -sf $WEBROOT/$TLD/config/$TLD.nginx.conf $NGINX_AVAILABLE_VHOSTS/$TLD.conf

##############################################################
# ENABLE SITE BY CREATING SYMBOLIC LINK TO SITES ENABLED #####
##############################################################
ln -sf $NGINX_AVAILABLE_VHOSTS/$TLD.conf $NGINX_ENABLED_VHOSTS/$TLD.conf


##############################################################
# RESTART NGINX AND SHOW STATUS ##############################
##############################################################
/etc/init.d/nginx restart;
ok "Site Created for $1"
/etc/init.d/nginx status