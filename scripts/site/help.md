

MODULE: SITE
============

www site list                         # Displays a list of all sites on the server
www site example.com --add            # Create a new site 
www site example.com --add (--ftp)    # With an FTP user
www site example.com --add (--wp)     # install Wordpress

www site example.com --DELETE         # Delete site  
www site example.com --DELETE (--ftp) # Delete site and FTP user

www site example.com --backup         # Backup site
www site example.com --RESTORE        # Restore backup

www site example.com --disable        # Remove config from Nginx sites-enabled 
www site example.com --enable         # Add config to NGINX sites-enabled

COMING...
www site example.com --add --port:3000 --ftp
www site example.com --add --proxy:s3.aws.amazon.com --ftp
    
`Note: Adding a site that already exists is non destructive so it can be used to rebuild the config file.`


