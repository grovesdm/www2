#!/usr/bin/env bash

#####################################################
# SET CONFIG FILE LOCATION ##########################
#####################################################
source "/etc/wwwrocket/rocket/default.conf"

# restart
	 if [ "$2" = 'restart'  ]; then
	sudo service apache2 restart

	echo -e "\033[32m
	  I have reloaded the Apache config for you..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# stop
	elif [ "$2" = 'stop'  ]; then
	sudo service apache2 stop

	echo -e "\033[32m
	  I have stopped the Apache Web Server for you..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# status
	elif [ "$2" = 'status' ]; then
	sudo service apache2 status

	echo -e "\033[32m
	  Thats the status of the Apache Web Server..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# Start
	elif [ "$2" = 'start' ]; then
	sudo service apache2 start

	echo -e "\033[32m
	  Starting the Apache Web Server..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"
	
	
# 	cdwww() { cd ${WEBROOT} }

# # restart
# elif [ "$1" = "cd" ]; then

# 	if [[ "$2" = "www" ]]; then
# 	cdwww
# 	echo $1 $2
# 	exit 1
	
# 	elif [[ "$2" = "backups" ]]; then
# 	cd "${BACKUP_FOLDER}"
# 	exit 1

# 	elif [[ "$2" = "passwords" ]]; then
# 	cd "${PASSWORDS_FOLDER}"
# 	exit 1


# 	elif [[ "$2" = "rocket" ]]; then
# 			cd "${WWWROCKET_DIRECTORY}"
# 			exit 1
# 	else
# 	echo fail
# 	fi
# 	fi  
	
	
	
	
	
	
	fi