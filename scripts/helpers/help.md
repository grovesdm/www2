MODULE: HELPERS | Web Services
====================
    www apache|nginx|web|db start          # Start services 
    www apache|nginx|web|db stop           # Stop services
    www apache|nginx|web|db test           # Test configuration
    www apache|nginx|web|db reload         # Reload services
    www apache|nginx|web|db restart        # Restart services
    www apache|nginx|web|db status         # Check status of services


