#!/bin/bash

# Skip apt-get update if its not a fresh install
if [ "$1" = '--update' ]; then

echo -e "\033[32m
Updating the ai firewall script..." &&

echo -e "\033[0m"

echo "Wont be too long...
"

dpkg -s netfilter-persistent fail2ban nmap 2>/dev/null >/dev/null || sudo apt-get -y install netfilter-persistent fail2ban nmap


else


# If first time update everything and install dependendies

echo -e "\033[32m
Doing a fresh install of the ai firewall script..." &&

echo -e "\033[0m"

echo "In the words of 'Roxette' - 'Hold on tight. You know she's a little bit dangerous...'
"

apt-get update && sudo apt-get install netfilter-persistent fail2ban nmap -y

fi


# Update

# Make the scripts directory if it doesn't exist (wont overwrite existing files)
mkdir -p /usr/local/bin/ai_scripts && mkdir -p /usr/local/bin/ai_scripts/firewall

# Download and install latest version of aif
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/aif.sh -O /usr/local/bin/ai_scripts/firewall/aif

#todo this is temp to get standalone working
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/ai-script.sh -O /usr/local/bin/ai
chmod +x /usr/local/bin/ai
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/ai.conf -O /usr/local/bin/ai_scripts/ai.conf
#todo ends

wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/db_server.sh -O /usr/local/bin/ai_scripts/firewall/db_server
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/web_server.sh -O /usr/local/bin/ai_scripts/firewall/web_server
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/reset.sh -O /usr/local/bin/ai_scripts/firewall/reset



# Make the backup directory if it doesnt already exist wont overwrite if it does exist
mkdir -p /etc/ai && mkdir -p /etc/ai/firewall

#	echo "
#	# Add passive FTP at boot
#	modprobe ip_conntrack_ftp
#	" >> /etc/modules

#Adds the module to startup file only if it doesnt exist already
grep -q -F "modprobe ip_conntrack_ftp" /etc/modules || echo "#modprobe ip_conntrack_ftp" >> /etc/modules

#todo I think the above will break ftp connection but I currently dont use it anyway.


clear &&

echo -e "\033[32m
Installation has of ip firewall has now completeed..." &&

echo -e "\033[0m"

echo "Ok thats done.... What now? Type ai --help for suggestions...
"

# Delete download from home directory (if exists)
rm -f ~/aifi
