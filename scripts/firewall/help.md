Firewall
========
    ai firewall --install               # fresh install of script and update of dependencies
    *ai firewall --UNINSTALL            # completely removes the ai firewall scripts
    ai firewall --update                # Updates firewall scripts
    ai firewall --reset                 # flushes all rules and saves config 
    ai firewall --web                   # saves the default config for web server
    ai firewall --db                    # saves the default config for db server
    ai firewall --status                # shows the current 'iptable' rules and open ports


    