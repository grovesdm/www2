#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"


if [ "$2" = '--install' ] || [ "$2" = '--update' ]; then
	wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/install.sh && bash ~/aifi "$2"

	else
	# All other firewall functions pass $var to aif
	# if installed pass to aif
		if [ -f "${SCRIPTS_DIRECTORY}/firewall/aif.sh" ]; then
		bash "${SCRIPTS_DIRECTORY}/firewall/aif.sh" "$1" "$2"

		# else warning
		else
		echo -e "\033[32m
		It looks like the firewall module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai firewall --install'
		"
		fi
	fi