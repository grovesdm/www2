#!/bin/bash

echo -e "\033[32m
Resetting the ai firewall iptables..." &&

echo -e "\033[0m"

echo "And other technical stuff...
"



# Stop the IP Tables persistent service
service iptables-persistent flush

#First, set the default policies for each of the built-in chains to ACCEPT. The main reason to do this is to ensure that you won’t be locked out from your server via SSH:
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

#Then flush the nat and mangle tables, flush all chains (-F), and delete all non-default chains (-X):
sudo iptables -t nat -F
sudo iptables -t mangle -F
sudo iptables -F
sudo iptables -X

#Your firewall will now allow all network traffic. If you list your rules now, you will will see there are none, and only the three default chains (INPUT, FORWARD, and OUTPUT) remain.

sudo invoke-rc.d netfilter-persistent save

# Start the IP Tables persistent service
service netfilter-persistent start

clear

echo -e "\033[32m
I setted the rules for you see..." && echo -e "\033[0m"

iptables -L

echo -e "\033[32m
And the Open Ports
==================" && echo -e "\033[0m"


sudo nmap localhost