vsFTP
=====
    ai ftp --install                    # install installs ftp and its dependencies
    ai ftp add example.com              # add a new ftp user example.com with access to /var/www/example.com
    ai ftp --DELETE example.com         # deletes ftp user example.com
    ai ftp password example.com         # (resets the ftp password for user gizmo.com)
    ai ftp list                         # opens list of all ftp users and passwords
    *ai ftp --UNINSTALL                 # completely removes the ai firewall scripts