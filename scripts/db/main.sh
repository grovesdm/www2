#!/usr/bin/env bash
#####################################################
# SET NAME VARIABLES FOR ERROR MESSAGES FROM CONFIG #
#####################################################
MODULE="DATABASE"
MODULE_CLI="db"
#####################################################
# SET CONFIG FILE LOCATION ##########################
#####################################################
source "/etc/wwwrocket/rocket/default.conf"


##################################################
# CHECK REQUIRED PACKAGES INSTALLED ##############
##################################################
#dpkg -s "mysqld" >/dev/null 2>&1 || echo shit

#ansible-playbook mysql.yml


# Needed to update if installed as stand alone
if [ "$2" = '--install' ] || [ "$2" = '--update' ]; then
	ansible-playbook "${ROCKET_DIRECTORY}/ansible/mysql.yml"
	exit 1

# Create a new database or create a new user for an existing DB
elif [ "$2" = "add" ] && [[ ! -z "$3" ]]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/db_add.sh" "$3"
	exit 1
# Delete a new database or create a new user for an existing DB
elif [ "$2" = "--DELETE" ] && [[ ! -z "$3" ]]; then


	bash "${SCRIPTS_DIRECTORY}/db/tasks/db_del.sh" "$3" "$4"
	exit 1


# List DB
elif [ "$2" = "--list" ]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/db_list.sh"
	exit 1
# List DB users
elif [ "$2" = "user" ] && [ "$3" = "--list" ]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/user_list.sh"
	exit 1
##########
##################################################
# DELETE DATABASE USER ###########################
##################################################
elif [ "$2" = "user" ] && [ "$3" = "--DELETE" ] && [[ ! -z "$4" ]]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/user_del.sh" "$4"
	exit 1
##################################################
# UPDATE DATABASE USER PASSWORD ##################
##################################################
elif [ "$2" = "user" ] && [ "$3" = "--PASSWORD" ]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/user_pass.sh" "$4"
	exit 1
##################################################
# ADD NEW USER TO DATABASE #######################
##################################################
elif [ "$2" = "user" ] && [[ "$3" = "add" ]] && [[ ! -z "$4" ]]; then
	bash "${SCRIPTS_DIRECTORY}/db/tasks/db_add.sh" "$4"
	exit 1















##################################################
# BACKUP DATABASE ###############################
##################################################
##### db backup ######################################################
elif [ "$2" = 'backup' ]; then
    ###################################
    # SANITY CHECK FOR BLANK VARIABLE #
    ###################################
    if [[ ! -z "$5" ]]; then
        echo -ne "${ERROR_MODULE_NO_UNDERSTAND}"
		echo -ne "${ERROR_MODULE_HELP}"
		exit 1
	##################################
    # SANITY CHECK FOR BLANK VARIABLE #
    ###################################
    elif [[ -z "$3" ]]; then
        echo -ne "${ERROR_MODULE_MISSING_VAR}"
		echo -ne "${ERROR_MODULE_HELP}"
		exit 1
    #############################
    # IF NOT BLANK EXECUTE TASK #
    #############################
    elif [[ "$3" = "--list" ]]; then
        bash "${SCRIPTS_DIRECTORY}/db/tasks/db_backup_list.sh" "$3"
        exit 1
    #############################
    # IF NOT BLANK EXECUTE TASK #
    #############################
    else
        bash "${SCRIPTS_DIRECTORY}/db/tasks/db_backup.sh" "$3" "$4"
        exit 1
    fi
##################################################
# RESTORE DATABASE ###############################
##################################################
elif [ "$2" = '--RESTORE' ]; then
    ###################################
    # SANITY CHECK FOR BLANK VARIABLE #
    ###################################
    if [[ ! -z "$4" ]]; then
        echo -ne "${ERROR_MODULE_NO_UNDERSTAND}"
		echo -ne "${ERROR_MODULE_HELP}"
		exit 1
	##################################
    # SANITY CHECK FOR BLANK VARIABLE #
    ###################################
    elif [[ -z "$3" ]]; then
        echo -ne "${ERROR_MODULE_MISSING_VAR}"
		echo -ne "${ERROR_MODULE_HELP}"
		exit 1
    #############################
    # IF NOT BLANK EXECUTE TASK #
    #############################
    else
        bash "${SCRIPTS_DIRECTORY}/db/tasks/db_restore.sh" "$3"
        exit 1
    fi
##################################################
# HELP ###########################################
##################################################
elif [ "$2" = '--help' ]; then
	###################################
    # SANITY CHECK FOR BLANK VARIABLE #
    ###################################
    if [[ ! -z "$3" ]]; then
        echo -ne "${ERROR_MODULE_NO_UNDERSTAND}"
		echo -ne "${ERROR_MODULE}"
		exit 1
    #############################
    # IF NOT BLANK EXECUTE TASK #
    #############################
    else
		clear
        cat "${SCRIPTS_DIRECTORY}/db/help.md"
        exit 1
    fi
else
##################################################
# IF ALL ELSE FAILS ##############################
##################################################
echo -ne "${ERROR_MODULE_NO_UNDERSTAND}"
echo -ne "${ERROR_MODULE}"
fi