

# HELP FOR MODULE: DATABASE

## INSTALLATION

    www db --install    # install db module
    www db --update     # update db module   

## DATABASE TASKS

    www db list                    # List available databases
    www db add dbname              # (non-destuctive if database exists. can also use to create a new database user)
    www db --DELETE dbname dbuser  # in order to delete a database you need to enter a valid database user

## USER TASKS
    
    www db user list               # list mysql users
    www db user add dbname         # add a new user to an existing database
    www db user --PASSWORD dbuser  # change password for the specified user
    www db user --DELETE dbuser    # delete user

## BACKUP/ RESTORE TASKS

    www db backup example.com       # Dumps a copy of the db backup folder  
    www db backup list              # Lists available database backups
    www db --RESTORE example.com    # restores the backup to database   
                                    # (Database needs to exist first)

    For more info please visit https://rkt.sh/db

## HELPERS

    www db stop      # stops the database server
    www db start     # starts the database server
    www db restart   # restarts the database status
    www db status    # shows the database status

    For more info please visit https://rkt.sh/helpers

