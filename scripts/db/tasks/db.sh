#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"


	# if [ "$2" = '--install' ]; then
	# # wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --install
	# elif [ "$2" = '--update' ]; then
	# # wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --update

	# run backup script if installed
	if [ "$2" = 'backup' ]; then
	if [ -d "../backup" ]; then
		bash "${SCRIPTS_DIRECTORY}/backup/db_backup.sh" "$3"

		# else warning
		else
		echo -e "\033[32m
		It looks like the backup module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai backup --install'
		"
		fi


	else
	# All other db functions pass $var to aif
	# if installed pass to aidb
		if [ -f "${SCRIPTS_DIRECTORY}/db/aidb.sh" ]; then
		bash "${SCRIPTS_DIRECTORY}/db/aidb.sh" "$2" "$3" "$4"

		# else warning
		else
		echo -e "\033[32m
		It looks like the db module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai db --install'
		"
		fi
fi