#!/usr/bin/env bash

#####################################################
# SET CONFIG FILE LOCATION ##########################
#####################################################
source "/etc/wwwrocket/rocket/default.conf"
#source /home/daniel/Documents/www2/default.conf

DB_BACKUP_DESCRIPTION=""

#DB=$3 #If i pass all vars the $DB helper doesnt work


if [[ $2 != "" ]]; then
  DB_BACKUP_DESCRIPTION="--$2"
fi

DB_BACKUP_NAME="$DB--$BACKUP_TIME$DB_BACKUP_DESCRIPTION.sql"

mysql -e "FLUSH TABLES WITH READ LOCK; SET GLOBAL read_only = ON;" &&

#mysqldump $1 > $DB_BACKUP_FOLDER$DB_BACKUP_NAME
mysqldump $DB > $DB_BACKUP_FOLDER/$DB_BACKUP_NAME

mysql -e "SET GLOBAL read_only = OFF; UNLOCK TABLES;"

echo "
Database $DB has been backed up dor site $TLD... $DB_BACKUP_NAME DESC: $DB_BACKUP_DESCRIPTION
"












