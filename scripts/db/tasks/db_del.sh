#!/bin/bash

source "/etc/wwwrocket/rocket/default.conf"

#user="root"
#password="xsv42k"
#now="$(date +"%Y-%m-%d %H:%M:%S")"
DATABASE="$(echo "$1" | sed -r 's/[.]+/_/g')"
EXISTS="$(mysqlshow $DATABASE | grep -v Wildcard | grep -o $DATABASE)"
USER_EXISTS="$(mysql -sse "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '$2')")"




###################################################################################
# MESSAGES ########################################################################
###################################################################################
ERROR_RUN_AS_ROOT="\n${RED}WWWROCKET ERROR =>${RESET} ${HIGHLIGHT} SORRY THIS SCRIPT MUST BE RUN AS SUDO OR ROOT ${RESET}\n\n"
ERROR_NO_USER="\n${YELLOW}WWWROCKET ERROR =>${RESET} ${HIGHLIGHT} SORRY YOU NEED TO INCLUDE A VALID DATABASE USER ${RESET}\n\n"
REMOVING_PACKAGES="\n${YELLOW} REMOVING EXISTING PACKAGES =>${RESET} ${HIGHLIGHT} PYTHON PIP ANSIBLE GIT ${RESET}\n\n"
INSTALLING_PYTHON="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING PYTHON & PIP ${RESET}\n\n"
INSTALLING_ANSIBLE="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING ANSIBLE WITH PIP ${RESET}\n\n"
INSTALLING_GIT="\n${YELLOW} PREPARING FOR LAUNCH =>${RESET} ${HIGHLIGHT} INSTALLING GIT ${RESET}\n\n"
HANDOVER_TO_WWWROCKET="\n${GREEN} PREPARING FOR LAUNCH => ${RESET}${HIGHLIGHT} HANDING OVER TO WWWROCKET INSTALLER ${RESET}\n\n"
###################################################################################



##################################################
# CHECK FOR NON EMPTY OR INCORRECT VARIABLE ######
##################################################
if [[ -z "$2" ]] ; then
    echo -ne "${ERROR_NO_USER}"
    exit 1
fi

#check the db exists
if [ "$EXISTS" == "$DATABASE" ]; then

#check the user exists
if [ $USER_EXISTS = 1 ]; then

mysql << EOF
DROP DATABASE $DATABASE;
DELETE FROM mysql.user WHERE User = '$2';
exit
EOF

rm -f $DB_PASSWORDS_FOLDER$2.txt

echo "Done and done" &&
echo "$NOW - Deleted database '$DATABASE' with user '$2'"

# If use no user exist
elif [ $USER_EXISTS = 0 ]; then
  echo "Sorry that database user doesn't exist...

database user $2

"

fi

#If db no exist
else
echo "
Sorry the database '$DATABASE' doesn't exist... Enter the correct name and user...
"
fi
