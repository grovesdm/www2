#!/usr/bin/env bash
#################################################
# LOAD CONFIG ###################################
#################################################
source "/etc/wwwrocket/rocket/default.conf"
#################################################
# MESSAGES ######################################
#################################################

DB_BACKUPS=( "$DB_BACKUP_FOLDER/$DB"*.sql )
DB_BACKUP_SELECT="\n${GREEN}  WWWROCKET =>${RESET} ${HIGHLIGHT} SELECT THE BACKUP YOU WANT TO RESTORE OR PRESS Q TO QUIT ${RESET}\n\n"
DB_BACKUP_NONE="\n${RED}  WWWROCKET =>${RESET} ${HIGHLIGHT} SORRY THERE ARE NO BACKUPS FOR THIS SITE DB [$DB] ${RESET} - MAYBE CHECK SPELLING? \n\n"
DB_BACKUP_SELECT_FAIL="\n${RED}  WWWROCKET =>${RESET} ${HIGHLIGHT} SORRY THE BACKUP YOU HAVE SELECTED DOES NOT EXIST. PLEASE SELECT AGAIN ${RESET}\n\n"
DB_RESTORE_CONFIRM="\n${GREEN}  WWWROCKET =>${RESET} ${HIGHLIGHT} CONFIRM YOU WANT TO RESTORE BACKUP FOR [$1]. IT WILL OVERWRITE IF IT EXISTS ${RESET}\n\n"
DB_RESTORE_CANCELLED="\n${YELLOW}  WWWROCKET =>${RESET} ${HIGHLIGHT} BACKUP RESTORATION CANCELLED ${RESET}\n\n"




#################################################
# CHECK IF A BACKUP EXISTS IN THE BACKUP FOLDER #
#################################################
if ls $DB_BACKUPS 1> /dev/null 2>&1; then
    #################################################
    # IF BACKUPS EXISTS ASK USER TO SELECT ##########
    #################################################
    echo -ne "$DB_BACKUP_SELECT"
    select DB_BACKUP in "${DB_BACKUPS[@]}"; do
        #################################################
        # IF USER QUITS #################################
        #################################################
        if [[ $REPLY =~ ^[Qq]$ ]]; then
            echo -ne "$RESTORE_CANCELLED" >&2
            exit 1
        #################################################
        # IF USER SELECTS INVALID OPTION ################
        #################################################
        elif [[ -z $DB_BACKUP ]]; then
            echo -ne "$DB_BACKUP_SELECT_FAIL" >&2
        #################################################
        else
            #################################################
            # CONFIRMATION BEFORE RESTORE ###################
            #################################################
            echo -ne "$DB_RESTORE_CONFIRM"
            #################################################
            # CALL CONFIRMATION FUNCTION ####################
            #################################################
            confirm
            ok "\n RESTORING $DB_BACKUP\n\n"




mysql -e "FLUSH TABLES WITH READ LOCK; SET GLOBAL read_only = ON;" &&

# USE VARS FROM THE CONFIG FILE
#################################################
mysql $DB < $DB_BACKUP &&

mysql -e "SET GLOBAL read_only = OFF; UNLOCK TABLES;"

echo "
Database $DB_BACKUP for database $DB for site $TLD has been restored ...


"











            #################################################
            # END ###########################################
            #################################################
            exit 1
            #################################################
        fi
    done
#################################################
# IF NO TLD BACKUPS EXIST #######################
#################################################
else
    echo -ne $DB_BACKUP_NONE
    exit 1
#################################################
fi








#!/bin/bash

# Load config file

#source "/etc/wwwrocket/rocket/default.conf"
#
## Check backup exists
#if [ -f "$DB_BACKUP_FOLDER/$DB_BACKUP_NAME" ]; then
#
#mysql -e "FLUSH TABLES WITH READ LOCK; SET GLOBAL read_only = ON;" &&
#
## USE VARS FROM THE CONFIG FILE
##################################################
#mysql $DB < $DB_BACKUP_FOLDER/$DB_BACKUP_NAME &&
#
#mysql -e "SET GLOBAL read_only = OFF; UNLOCK TABLES;"
#
#echo "
#Database $DB_BACKUP_NAME for database $DB for site $TLD has been restored ...
#
#
#"
#
#
#
#		# else warning
#		else
#		echo -e "\033[32m
#		It looks like the backup $DB_BACKUP_NAME doesn't exist..."
#
#		echo -e "\033[0m"
#
#		echo "  You need to type the name exactly as it is called without the .sql
#
#		"
#		fi
