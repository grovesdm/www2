#!/usr/bin/env bash

source "/etc/wwwrocket/rocket/default.conf"


echo -e "\033[32m
BACKUPS IN THE BACKUP FOLDER
======================="
echo -e "\033[0m"

ls "${DB_BACKUP_FOLDER}" -I html -I index.html | col -b
echo -ne "\n"
