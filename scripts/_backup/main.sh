#!/usr/bin/env bash

# LINK TO CONFIG FILE
source "/etc/wwwrocket/rocket/default.conf"



##### htdocs backup ######################################################
if [ "$1" = 'backup' ] && [ "$2" = 'site' ]; then

bash "${SCRIPTS_DIRECTORY}/backup/tasks/htdocs_backup.sh" "$3"



##### htdocs restore ######################################################
elif [ "$1" = 'restore' ] && [ "$2" = 'site' ]; then

bash "${SCRIPTS_DIRECTORY}/backup/tasks/htdocs_restore.sh" "$3"


fi
