

MODULE: BACKUP RESTORE
======================

    USAGE:  www module site.com
        
        www [backup|restore] [database|db] example.com   # Backup or restore database for site example.com
        www [backup|restore] site example.com            # Backup or restore site folder for example.com
 
    EXAMPLES:
        
        www backup site example.com     # Back site example.com
        www restore site example.com    # Restore site example.com
        www backup db example.com       # Backup mysql database example.com

For more info please visit https://rkt.sh/backup

