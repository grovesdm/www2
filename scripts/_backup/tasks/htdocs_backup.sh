#!/usr/bin/env bash
################
# FILE LOCATIONS
################

source "/etc/wwwrocket/rocket/default.conf"

#Check if site folder exists
if [ -d "$WEBROOT/$TLD" ]; then

warn "
  Deleting old backup first if there is one...
  " &&

rm -f "$SITE_BACKUP_FOLDER/$SITE_BACKUP_NAME" &&

# Remove iwp backups #todo wtf is this?
#find $WEBROOT/tt.slyfoxmedia.com.au/htdocs/wp-content/infinitewp/backups/ -type f -not -name '*.php' -delete

if [ -d "$WEBROOT/$TLD/htdocs/wp-content/infinitewp/backups" ]; then

find $WEBROOT/$TLD/htdocs/wp-content/infinitewp/backups/ -type f -not -name '*.php' -delete -nowarn

fi


cd $WEBROOT && zip -r "$SITE_BACKUP_FOLDER/$SITE_BACKUP_NAME" $TLD &&

echo "
  Done... You now have a fresh backup of $TLD here '$SITE_BACKUP_FOLDER/$SITE_BACKUP_NAME'
"


else
# If no exist
die "
  Sorry site folder $WEBROOT/$TLD doesnt exits...
  "

fi