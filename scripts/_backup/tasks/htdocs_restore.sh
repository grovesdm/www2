#!/usr/bin/env bash

#################################################
# LOAD CONFIG ###################################
#################################################
source "/etc/wwwrocket/rocket/default.conf"

#################################################
# CONFIRMATION ##################################
#################################################
confirm "Are you sure you want to restore site $1?" "It will overwrite the site if it exist!"

# checks for existing backup file
if [ -f "$SITE_BACKUP_FOLDER/$SITE_BACKUP_NAME" ]; then

www site $1 --add
#ai site add $1

ok "Restoring $SITE_BACKUP_NAME to site $1...  "

# go to home directory in case the user is in the site folder
cd ~ &&

# Delete existing files so removed files arent replaced.
rm -fr "$WEBROOT/$TLD" &&

# extract to the site folder
cd $SITE_BACKUP_FOLDER && unzip -o $SITE_BACKUP_NAME -d $WEBROOT

ok "
OK I done a clean restore of the backup $BACKUP_FOLDER to $WEBROOT/$TLD....
"

# SET SITE AND WEB ROOT FILE PERMISSIONS
bash "${SCRIPTS_DIRECTORY}/site/tasks/apache/site-permissions.sh" $TLD

#ai apache reload
service apache2 restart && service nginx reload

else
# If backup no exits

die "
  Sorry backup $SITE_BACKUP_NAME for site $TLD doesn't exists...
  "

fi